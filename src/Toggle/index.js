import React from 'react';

class Toggle extends React.Component {
    constructor(props) {
        super(props);
        this.state = { value: '', response: '' };
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
    }
    handleSubmit(event) {
        event.preventDefault();
        let req = "";
        if (this.state.value === "admin") {
            req = "Dashboard"
        }
        else {
            req = "Login"
        }
        this.setState({ response: req });
    }

    render() {
        return (
            <div>
                <form onSubmit={(e) => this.handleSubmit(e)}>
                    <input type="text" value={this.state.value} onChange={(e) => this.handleChange(e)} />
                    <input type="submit" value="Submit" />
                </form>
                <p>
                    {this.state.response}
                </p>
            </div>
        );
    }
}

export default Toggle;