import axios from 'axios';
import React from 'react';
// import Image from '../../Atom/Images';
// import Label from '../../Atom/Label';
import { Table } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

// let UserInfo = (props) => {
//     let url;
//     if (props.gender) {
//         url = "https://pickaface.net/gallery/avatar/20150903_153704_412_Demo.png";
//     }
//     else {
//         url = "https://img.okezone.com/content/2020/03/18/206/2185025/corona-serang-selandia-baru-syuting-avatar-dihentikan-WffEkB3ElW.jpg";
//     }
//     return (
//         <div className="UserInfo">
//             <Image url={url} alt="hehe"></Image>
//             <div className="UserInfo-name">
//                 Marzota Dwi Rahmansyah
//             </div>
//             <Label>Hi, I Try to Learn React JS.</Label>
//             <Label>{Date()}</Label>
//         </div>
//     )
// }

// class UserInfo extends React.Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             url: ""
//         }
//     }

//     componentDidMount() {
//         if (this.props.gender) {
//             this.setState({
//                 url: "https://pickaface.net/gallery/avatar/20150903_153704_412_Demo.png"
//             })
//         }
//         else {
//             this.setState({
//                 url: "https://img.okezone.com/content/2020/03/18/206/2185025/corona-serang-selandia-baru-syuting-avatar-dihentikan-WffEkB3ElW.jpg"
//             })
//         }
//     }

//     render() {
//         return (
//             <div className="UserInfo">
//                 <Image url={this.state.url} alt="hehe"></Image>
//                 <div className="UserInfo-name">
//                     Marzota Dwi Rahmansyah
//                 </div>
//                 <Label>Hi, I Try to Learn React JS.</Label>
//                 <Label>{Date()}</Label>
//             </div>
//         )
//     }
// }

class UserInfo extends React.Component {
    constructor() {
        super();
        this.state = {
            name: "React"
        };
        this.getTodos = this.getTodos.bind(this);
    }

    componentDidMount() {
        this.getTodos();
    }

    async getTodos() {
        let data = await axios
            .get("https://jsonplaceholder.typicode.com/todos?_limit=10")
            .then(function (response) {
                return response;
            })
            .catch(function (error) {
                console.log(error);
            });
        this.setState({ todos: data.data });
    }
    render() {
        const { todos } = this.state;
        return (
            <div>
                <h3>using componentDidMount for initial data render</h3>
                <hr />
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Name</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    {todos && todos.map(todo => {
                        return (
                            <tbody>
                                <tr>
                                    <td>{todo.id}</td>
                                    <td>
                                        <p key={todo.id}>{todo.title}</p>
                                    </td>
                                    <td>
                                        <p>{todo.userId}</p>
                                    </td>
                                    <td>
                                        <p>{todo.completed ? '✔' : '❌'}</p>
                                    </td>
                                </tr>
                            </tbody>
                        )
                    })}
                </Table>
            </div>
        )
    }
}

export default UserInfo;