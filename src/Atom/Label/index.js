let Label = ({children}) => {
    return (
        <div className="Comment-Text">
            {children}
        </div>
    )
}

export default Label;