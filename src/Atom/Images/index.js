function Image(props) {
    return (
        <img className="Avatar" src={props.url} alt={props.alt} />
    )
}

export default Image;